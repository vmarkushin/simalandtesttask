//
//  ViewController.swift
//  TestApp
//
//  Created by Mac User on 23/04/2019.
//  Copyright © 2019 Mac User. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    let reuseIdentifier = "category_cell";
    var subcategoryTableViewController: SubcategoryTableViewController!
    var items: [CategoryItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.items.isEmpty {
            categoryRequest()
        }
        self.navigationItem.title = "Catalog"
        subcategoryTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "subcategories_table_controller") as? SubcategoryTableViewController
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.items[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CategoryCollectionViewCell
        cell.titleLabel.text = item.title
        cell.backgroundImageView.image = item.image
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        subcategoryTableViewController.path = items[indexPath.item].path
    self.navigationController?.pushViewController(subcategoryTableViewController, animated: true)
    }
    
    func categoryRequest() {
        let url = "https://www.sima-land.ru/api/v3/category/?is_not_empty=1&with_adult=1&page=1&level=1&sort=priority_home&expand-root=1&expand=name_alias"
        APIParser.httpRequest(url: url) { json in
            do {
                guard let jsonResponse = json as? [String: Any] else {
                    AppDelegate.showAlert(vc: self, text: "JSON parse error")
                    return
                }
                guard let jsonItems = jsonResponse["items"] as? [[String: Any]] else {
                    AppDelegate.showAlert(vc: self, text: "JSON parse error")
                    return
                }
                
                for item in jsonItems {
                    let photoURL = item["photo"] as! String
                    let title = item["name"] as! String
                    let path = item["path"] as! String
                    
                    let image = UIImage.init(data: try Data.init(contentsOf: URL(string: photoURL)!))!
                    let categoryItem = CategoryItem(title: title, image: image, path: path)
                    self.items.append(categoryItem)
                }
                
                DispatchQueue.main.async {
                    self.categoryCollectionView.reloadData()
                }
            } catch let parsingError {
                print("JSON parse error ", parsingError)
                AppDelegate.showAlert(vc: self, text: "JSON parse error")
            }
        }
    }
}
