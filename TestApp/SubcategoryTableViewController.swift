//
//  SubcategoryTableViewController.swift
//  TestApp
//
//  Created by Mac User on 23/04/2019.
//  Copyright © 2019 Mac User. All rights reserved.
//

import Foundation
import UIKit

class SubcategoryTableViewController : UITableViewController {
    var items: [String] = []
    let reuseIdentifier = "subcategory_cell"
    var path: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.showSpinner(onView: self.tableView)
        items = []
        subcategoryRequest()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.items[indexPath.item]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath)
        cell.textLabel?.text = item
        return cell
    }
    
    func subcategoryRequest() {
        let url = "https://www.sima-land.ru/api/v3/category/?is_not_empty=1&with_adult=1&page=1&level=2&sort=priority_home&expand-root=1&expand=name_alias&path=\(path)"
        APIParser.httpRequest(url: url) { json in
            guard let jsonResponse = json as? [String: Any] else {
                AppDelegate.showAlert(vc: self, text: "JSON parse error")
                return
            }
            guard let jsonItems = jsonResponse["items"] as? [[String: Any]] else {
                AppDelegate.showAlert(vc: self, text: "JSON parse error")
                return
            }
            
            for item in jsonItems {
                let title = item["name"] as! String
                self.items.append(title)
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            AppDelegate.removeSpinner()
        }
    }
}
