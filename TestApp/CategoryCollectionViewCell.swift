//
//  CategoryCollectionViewCell.swift
//  TestApp
//
//  Created by Mac User on 23/04/2019.
//  Copyright © 2019 Mac User. All rights reserved.
//

import Foundation
import UIKit

class CategoryCollectionViewCell : UICollectionViewCell {
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
