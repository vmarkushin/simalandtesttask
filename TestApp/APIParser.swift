//
//  APIParser.swift
//  TestApp
//
//  Created by Mac User on 23/04/2019.
//  Copyright © 2019 Mac User. All rights reserved.
//

import Foundation
import UIKit

class APIParser {
    static func httpRequest(url: String, callback: @escaping (Any?) -> ()) {
        let url = URL(string: url)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard let data = data, error == nil else {
                print(error ?? "error")
                return
            }
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [])
                callback(jsonResponse)
            } catch let parsingError {
                print("JSON parse error ", parsingError)
            }
        }
        
        task.resume()
    }
}
