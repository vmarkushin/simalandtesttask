//
//  CategoryItem.swift
//  TestApp
//
//  Created by Mac User on 23/04/2019.
//  Copyright © 2019 Mac User. All rights reserved.
//

import UIKit

struct CategoryItem {
    var title: String
    var image: UIImage
    var path: String
}
